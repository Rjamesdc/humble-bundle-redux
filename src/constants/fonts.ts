const FONT = {
    light: "Montserrat-Light",
    regular: "Alata-Regular",
    bold: "Montserrat-Bold",
    lightItalic: "Montserrat-LightItalic",
    italic: "Sarina-Regular",
    boldItalic: "Montserrat-BoldItalic",
    cursive: 'Streetwear'
  };
  
  export default FONT;
  