import { Dimensions } from 'react-native';
import COLOR from './colors';
import FONT from './fonts';
const { width, height } = Dimensions.get("window");

const SIZES = {
    width,
    height
}

export {
    SIZES,
    COLOR,
    FONT
}
