import React, { Component } from "react";
import {
  Text,
  View,
  TouchableOpacity,
  Image,
  TextInput,
  StyleSheet,
  ScrollView,
  Dimensions,
  StatusBar,
  ActivityIndicator,
} from "react-native";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import COLOR from "../constants/colors";
import FONT from "../constants/fonts";
const { width, height } = Dimensions.get("window");

import { types } from "../redux/Auth/types";
import { connect } from "react-redux";
import * as Navigation from "../navigation/RootNavigation";

import HumbleHeader from "../components/HumbleHeader";
import VerticalSeparator from "../components/VerticalSeparator";
import Spacer from "../components/Spacer";
import HumbleFooter from "../components/HumbleFooter";
import AsyncStorage from "@react-native-async-storage/async-storage";

interface Props {
  login: (loginDetails: any) => void;
  navigation: any;
  error: {
    status: number,
    message: string
  }
  isLoading: boolean,
}

interface State{
  username: string,
  password: string,
}

export class Login extends Component<Props, State> {
  unsubscribe: any;
  constructor(props: Props) {
    super(props);
    this.state = {
      username:'',
      password:'',
    }
  }

  componentDidMount(){
    this.unsubscribe = this.props.navigation.addListener('focus', () => {
      this.LazyLogin()
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  LazyLogin = async () => {
    this.setState({
      username:'rjamesdelacruz@gmail.com',
      password:'12345'
    })
  }
  
  loginOnClick = (loginDetails: {}) => {
    const { login } = this.props;
    login(loginDetails)
    this.setState({
      username:'',
      password: ''
    })
  } 

  render() {
    const { error, isLoading } = this.props;
    const { username, password } = this.state;
    return (
      <React.Fragment>
        <StatusBar
          barStyle={"light-content"}
          translucent={true}
          backgroundColor={COLOR.HEADER_BACKGROUND}
        />
        <ScrollView>
          <View style={styles.container}>
            <HumbleHeader title="Humble" backBtn={false} showMenu={false}/>
            <View style={styles.containerBox}>
              <Text style={styles.loginText}>Login</Text>
              <View style={styles.inputBox}>
                <Icon
                  name="email-outline"
                  style={styles.iconStyle}
                  size={30}
                  color={COLOR.GRAY}
                />
                <VerticalSeparator spacing={5} size={30} />
                <TextInput
				          value={username}
                  keyboardType="twitter"
                  placeholder="Username"
                  style={styles.textInputs}
                  onChangeText={(usernameText:string) => {
                    this.setState({ username: usernameText})
                  }}
                />
              </View>
              <Spacer space={5} />
              <View style={styles.inputBox}>
                <Icon
                  name="lock"
                  style={styles.iconStyle}
                  size={30}
                  color={COLOR.GRAY}
                />
                <VerticalSeparator spacing={5} size={30} />
                <TextInput
				          value={password}
                  secureTextEntry
                  placeholder="Password"
                  style={styles.textInputs}
                  onChangeText={(passwordText:string)=>{
                    this.setState({ password: passwordText})
                  }}
                />
              </View>
              {
                error && <Text style={{
                  color: COLOR.RED,
                  alignSelf: 'center'
                }}>{error.message}</Text>
              }
              <TouchableOpacity onPress={() => console.log("Forgot Password")}>
                <Text style={styles.forgotPasswordText}>Forgot Password?</Text>
              </TouchableOpacity>
              <Spacer space={2} />
              <TouchableOpacity onPress={() => {
                const loginDetails = {
                  email: username,
                  password
                }
                this.loginOnClick(loginDetails)
              }}>
                {
                  isLoading ? <View><ActivityIndicator size='small' color={COLOR.BLUE} /></View>
                  : <Text style={styles.loginBtn}> LOGIN </Text>
                }
              </TouchableOpacity>
              <Spacer space={5} />
              <View>
                <Spacer space={10} />
                <View
                  style={{
                    backgroundColor: COLOR.GRAY,
                    width: "100%",
                    height: 1,
                  }}
                />
                <Text
                  style={{
                    backgroundColor: COLOR.WHITE,
                    position: "absolute",
                    textAlign: "center",
                    paddingHorizontal: 10,
                    top: 10,
                    alignSelf: "center",
                  }}
                >
                  or
                </Text>
                <Spacer space={15} />
              </View>
              <TouchableOpacity onPress={() => console.log("Login")}>
                <View style={{ flexDirection: "row" }}>
                  <Image
                    source={require("../assets/images/google.png")}
                    style={{
                      width: 30,
                      height: 30,
                      zIndex: 999,
                      top: 10,
                      position: "absolute",
                      left: 40,
                    }}
                  />
                  <Text
                    style={[
                      styles.loginBtn,
                      {
                        backgroundColor: COLOR.LIGHT_GRAY,
                        fontFamily: FONT.bold,
                        color: COLOR.BLACK,
                      },
                    ]}
                  >
                    LOG IN WITH GOOGLE
                  </Text>
                </View>
              </TouchableOpacity>
              <Spacer space={5} />
              <TouchableOpacity onPress={() => console.log("Facebook")}>
                <View style={{ flexDirection: "row" }}>
                  <Image
                    source={require("../assets/images/facebook.png")}
                    style={{
                      width: 30,
                      height: 30,
                      zIndex: 999,
                      top: 10,
                      position: "absolute",
                      left: 40,
                    }}
                  />
                  <Text
                    style={[
                      styles.loginBtn,
                      {
                        backgroundColor: COLOR.FACEBOOK,
                        fontFamily: FONT.bold,
                      },
                    ]}
                  >
                    LOG IN WITH FACEBOOK
                  </Text>
                </View>
              </TouchableOpacity>
              <Spacer space={5} />
            </View>
            <Text style={{ fontFamily: FONT.bold, fontSize: 15 }}>
              {`Don't have an account?`.toUpperCase()}
            </Text>
            <Spacer space={10} />
            <TouchableOpacity onPress={() => Navigation.navigate("SignUp", {})}>
              <Text
                style={[
                  styles.outerBtn,
                  {
                    borderWidth: 2,
                    borderColor: COLOR.BLACK,
                    fontFamily: FONT.bold,
                  },
                ]}
              >
                CREATE HUMBLE ACCOUNT
              </Text>
            </TouchableOpacity>
            <Spacer space={10} />
            <HumbleFooter />
          </View>
        </ScrollView>
      </React.Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: COLOR.CUSTOM_GRAY,
  },
  textInputs: {
	width: "85%", 
	height: "80%", 
	paddingLeft: 10
  },
  iconStyle: {
    alignItems: "center",
  },
  forgotPasswordText: {
    alignSelf: "center",
    textDecorationLine: "underline",
    marginVertical: 10,
  },
  loginText: {
    fontSize: 20,
    alignSelf: "center",
    fontFamily: FONT.bold,
    marginBottom: 10,
  },
  loginBtn: {
    alignSelf: "center",
    color: COLOR.WHITE,
    paddingVertical: 15,
    width: "100%",
    textAlign: "center",
    backgroundColor: COLOR.BLUE, 
    fontFamily: FONT.bold
  },
  outerBtn: {
    alignSelf: "center",
    color: COLOR.BLACK,
    paddingVertical: 15,
    width: width * 0.85,
    textAlign: "center",
  },
  inputBox: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 10,
    paddingVertical: 2,
    borderWidth: 1.5,
    borderColor: COLOR.BORDER_COLOR,
  },
  containerBox: {
    backgroundColor: COLOR.WHITE,
    width: width * 0.95,
    marginVertical: 10,
    borderColor: COLOR.BORDER_COLOR,
    borderWidth: 1.5,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
});

const mapStateToProps = (state: any) => ({
  userInfo: state.authReducer.userInfo,
  error: state.authReducer.error,
  isLoading: state.authReducer.isLoading,
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    login: (loginDetails:any) => {
      dispatch({
        type: types.GET_USER,
        payload: loginDetails
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
