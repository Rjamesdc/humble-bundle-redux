import React, { Component } from 'react'
import { View, Text, StyleSheet, TextInput, TouchableOpacity, ActivityIndicator } from 'react-native'
import { connect } from 'react-redux'
import { COLOR, FONT, SIZES } from '../constants'
import IconButton from '../components/IconButton'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons'
import * as Navigation from '../navigation/RootNavigation'
import HumbleHeader from '../components/HumbleHeader'
import { types } from '../redux/Auth/types';
import CustomBackButton from '../components/CustomBackButton'

interface Props {
    isLoading: boolean,
    error: {
        status: any
    },
    success: {
        status: any
    },
    userInfo: any,
    updateProfile: any,
    clearMessages: any
}

interface State{
    fullName: string,
    address: string,
    image: string,
    background: string,
    mobileNumber: string,
    email: string,
    password: string,
}

export class UpdateProfile extends Component<Props, State>{
    constructor(props:Props){
        super(props);
        const { userInfo } = this.props;

        this.state = {
            fullName: userInfo.fullname,
            address: userInfo.address,
            image: userInfo.image,
            background: userInfo.background,
            email: userInfo.email,
            mobileNumber: userInfo.mobilenumber,
            password: userInfo.password,
        }
    }


    handleTextChange = (text:string, field:string) => {
        switch (field) {
            case "FullName":
                return this.setState({ fullName: text })     
            case "Email":
                return this.setState({ email: text })
            case "Password":
                return this.setState({ password: text })   
            case "MobileNumber":
                return this.setState({ mobileNumber: text })      
            case "Address":
                return this.setState({ address: text })    
            default:
                break;
        }
    }

    updateProfileOnClick = () => {
        const { fullName, email, password, mobileNumber, image, background, address } = this.state;
        const { userInfo, updateProfile } = this.props
        const credentials = {
            fullName,
            email,
            password,
            mobileNumber,
            image,
            background,
            address,
            token: userInfo.token,
        }
        updateProfile(credentials);
    }

    clearApiMessages = () => {
        const { clearMessages } = this.props;
        clearMessages()
        Navigation.goBack()
    }

    render() {
        const { userInfo, isLoading, success, error } = this.props
        const { fullName, address, email, mobileNumber, password } = this.state;

        return (
            <View style={styles.container}>
                <CustomBackButton customAction={this.clearApiMessages}/>
                {
                        success.status === 200 &&
                        <View style={{  
                            flexDirection: 'row',
                            width: SIZES.width,
                            backgroundColor: COLOR.GREEN,
                            paddingHorizontal: 20,
                            paddingVertical: 5,
                            marginBottom: 10,
                            alignItems: 'center'
                        }}>
                            <Text style={{
                                color: COLOR.WHITE,
                                paddingRight: 10

                            }}>Profile Successfully Updated</Text>
                            <Icon 
                            name="check-circle"
                            size={15}
                            color={COLOR.WHITE}
                            />
                        </View>
                    }
                {
                    error.status == 422 && 
                    <View style={{  
                        flexDirection: 'row',
                        width: SIZES.width,
                        backgroundColor: COLOR.RED,
                        paddingHorizontal: 20,
                        paddingVertical: 5,
                        marginBottom: 10,
                        alignItems: 'center'
                    }}>
                        <Text style={{
                            color: COLOR.WHITE,
                            paddingRight: 10

                        }}>Error while updating profile. Please try again</Text>
                        <Icon 
                        name="alert-circle"
                        size={15}
                        color={COLOR.WHITE}
                        />
                    </View> 

                }
                <View style={{
                    paddingHorizontal: 20
                }}>
                    <Text style={{
                        fontFamily: FONT.bold,
                        color: COLOR.WHITE,
                        fontSize: 20
                    }}>Account Details:</Text>
                    <View style={{
                        marginTop: 20
                    }}>
                        <Text style={{
                            color: COLOR.WHITE,
                            fontSize: 12
                        }}>Full Name:</Text>
                        <TextInput value={fullName} onChangeText={(text) => this.handleTextChange(text, "FullName")} placeholder="Full Name" placeholderTextColor={COLOR.GRAY} style={{
                         color: COLOR.WHITE,
                         fontSize: 15
                         }}/>
                            <Text style={{
                            color: COLOR.WHITE,
                            fontSize: 12
                        }}>Email:</Text>
                        <TextInput value={email} onChangeText={(text) => this.handleTextChange(text, "Email")} placeholder="Email Address" placeholderTextColor={COLOR.GRAY} style={{
                         color: COLOR.WHITE,
                         fontSize: 15
                         }}/>
                            <Text style={{
                            color: COLOR.WHITE,
                            fontSize: 12
                        }}>Password:</Text>
                        <TextInput value={password} onChangeText={(text) => this.handleTextChange(text, "Password")} placeholder="Password" secureTextEntry placeholderTextColor={COLOR.GRAY} style={{
                         color: COLOR.WHITE,
                         fontSize: 15
                         }}/>
                        <Text style={{
                            color: COLOR.WHITE,
                            fontSize: 12
                        }}>Mobile Number:</Text>
                        <TextInput value={mobileNumber} onChangeText={(text) => this.handleTextChange(text, "MobileNumber")} placeholder="Mobile Number" placeholderTextColor={COLOR.GRAY} style={{
                         color: COLOR.WHITE,
                         fontSize: 15
                         }}/>
                        <Text style={{
                            color: COLOR.WHITE,
                            fontSize: 12
                        }}>Address:</Text>
                        <TextInput value={address} onChangeText={(text) => this.handleTextChange(text, "Address")} placeholder="Address" placeholderTextColor={COLOR.GRAY} style={{
                         color: COLOR.WHITE,
                         fontSize: 15
                         }}/>
                    </View>
                    <TouchableOpacity 
                    onPress={() => this.updateProfileOnClick()}
                    style={{
                        marginTop: 40
                    }}>
                        <Text style={{
                            backgroundColor: COLOR.BLUE,
                            paddingVertical: 20,
                            paddingHorizontal: 20,
                            textAlign:'center',
                            borderRadius: 20,
                            fontFamily: FONT.bold,
                            color: COLOR.WHITE
                        }}>
                            {
                                isLoading ? 
                                <ActivityIndicator size="small" color={COLOR.WHITE}/>
                                : 'Update'
                            }
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: COLOR.HEADER_BACKGROUND,
    }
})

const mapStateToProps = (state:any) => ({
    isLoading: state.authReducer.isLoading,
    userInfo: state.authReducer.userInfo,
    success: state.authReducer.success,
    error: state.authReducer.error
})

const mapDispatchToProps = (dispatch:any) => {
    return{
        updateProfile: (credentials:any) => {
            dispatch({
                type: types.UPDATE_PROFILE,
                payload: credentials
            })
        },
        clearMessages: () => {
            dispatch({
                type: types.UPDATE_PROFILE_CLEAR
            })
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdateProfile)
