import React, { Component } from "react";
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Image,
  KeyboardAvoidingView,
  ScrollView,
} from "react-native";
import { connect } from "react-redux";
import FONT from "../constants/fonts";
import COLOR from "../constants/colors";
const { width, height } = Dimensions.get("window");
import * as Navigation from "../navigation/RootNavigation";
import HumbleHeader from "../components/HumbleHeader";
import Spacer from "../components/Spacer";
import HumbleFooter from "../components/HumbleFooter";
import TextInputField from '../components/TextInputField';
import { types } from '../redux/Auth/types'

interface Props {
  register: (accountDetails: any) => void
  registerOnClick: () => void
}

interface State {
  fullName: string,
  email: string,
  mobileNumber: string,
  address: string,
  password: string,
  confirmPassword: string,
  errorHandlers: any
}

export class SignUp extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      fullName: "",
      email: "",
      mobileNumber: "",
      address: "",
      password: "",
      confirmPassword: "",
      errorHandlers: {
        fullName: false,
        email: false,
        mobileNumber: false,
        address: false,
        password: false,
        confirmPassword: false,
        passwordNotTheSame: false
      },
    }
  }

  componentWillUnmount() {
    this.cleanStates()
  }

  cleanStates = () => {
    const { errorHandlers } = this.state;
    errorHandlers["fullName"] = false;
    errorHandlers["email"] = false;
    errorHandlers["mobileNumber"] = false;
    errorHandlers["address"] = false;
    errorHandlers["password"] = false;
    errorHandlers["confirmPassword"] = false;
    errorHandlers["passwordNotTheSame"] = false;

    this.setState({
      fullName: "",
      email: "",
      mobileNumber: "",
      address: "",
      password: "",
      confirmPassword: "",
      errorHandlers
    })
  }


  textInputOnChange = (text: string, fieldName: string) => {
    switch (fieldName) {
      case "FullName":
        return this.setState({ fullName: text })
      case "EmailAddress":
        return this.setState({ email: text })
      case "MobileNumber":
        return this.setState({ mobileNumber: text })
      case "Address":
        return this.setState({ address: text })
      case "Password":
        return this.setState({ password: text })
      case "ConfirmPassword":
        return this.setState({ confirmPassword: text })
      default:
        break;
    }
  }

  setErrorValidations = (credentials: any) => {
    const { errorHandlers } = this.state;
    const { fullName, email, mobileNumber, address, password, confirmPassword } = credentials;

    errorHandlers["fullName"] = fullName.length === 0;
    errorHandlers["email"] = email.length === 0;
    errorHandlers["mobileNumber"] = mobileNumber.length === 0;
    errorHandlers["address"] = address.length === 0;
    errorHandlers["password"] = password.length === 0;
    errorHandlers["confirmPassword"] = confirmPassword.length === 0;
    errorHandlers["passwordNotTheSame"] = confirmPassword !== password;

    this.setState({ errorHandlers });
  }

  registerOnClick = () => {
    const { register } = this.props; //Props from redux
    const { fullName, email, mobileNumber, address, password, confirmPassword } = this.state;
    const credentials = {
      fullName,
      email,
      password,
      mobileNumber,
      address,
      confirmPassword
    }
    this.setErrorValidations(credentials)
    if (fullName.length === 0 || email.length === 0 || mobileNumber.length === 0 || address.length === 0 ||
      password.length === 0 || confirmPassword.length === 0 || confirmPassword !== password) return;

    register(credentials);
  }

  render() {
    const { errorHandlers } = this.state;
    return (
      <>
        <HumbleHeader title="Humble" backBtn={true} showMenu={false} />
        <View style={styles.container}>
          <KeyboardAvoidingView style={{ flexGrow: 1, height: '100%' }} behavior="height">
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={styles.containerBox}>
                <Text
                  style={{
                    fontSize: 20,
                    alignSelf: "center",
                    fontFamily: FONT.bold,
                    marginBottom: 10,
                  }}
                >
                  Create an Account
                </Text>
                <TextInputField
                  maxLength={30}
                  keyboardType="twitter"
                  textInputChange={this.textInputOnChange}
                  iconColor={COLOR.GRAY}
                  iconSize={30}
                  iconName="account"
                  placeholder="FullName"
                  isSecure={false}
                  fieldName="FullName"
                  isError={errorHandlers.fullName}
                />
                {
                  errorHandlers.fullName && <Text style={styles.textError}>Full Name is required.</Text>
                }
                <Spacer space={5} />
                <TextInputField
                  maxLength={30}
                  keyboardType="email-address"
                  textInputChange={this.textInputOnChange}
                  iconColor={COLOR.GRAY}
                  iconSize={30}
                  iconName="email-outline"
                  placeholder="Email Address"
                  fieldName="EmailAddress"
                  isError={errorHandlers.email}
                />
                {
                  errorHandlers.email && <Text style={styles.textError}>Email Address is required.</Text>
                }
                <Spacer space={5} />
                <TextInputField
                  maxLength={11}
                  keyboardType="phone-pad"
                  textInputChange={this.textInputOnChange}
                  iconColor={COLOR.GRAY}
                  iconSize={30}
                  iconName="cellphone"
                  placeholder="Mobile Number"
                  isSecure={false}
                  fieldName="MobileNumber"
                  isError={errorHandlers.mobileNumber}
                />
                {
                  errorHandlers.mobileNumber && <Text style={styles.textError}>Mobile Number is required.</Text>
                }
                <Spacer space={5} />
                <TextInputField
                  maxLength={100}
                  keyboardType="default"
                  textInputChange={this.textInputOnChange}
                  iconColor={COLOR.GRAY}
                  iconSize={30}
                  iconName="home"
                  placeholder="Address"
                  isSecure={false}
                  fieldName="Address"
                  isError={errorHandlers.address}
                />
                {
                  errorHandlers.address && <Text style={styles.textError}>Address is required.</Text>
                }
                <Spacer space={5} />
                <TextInputField
                  maxLength={26}
                  keyboardType="default"
                  textInputChange={this.textInputOnChange}
                  iconColor={COLOR.GRAY}
                  iconSize={30}
                  iconName="lock"
                  placeholder="Password"
                  isSecure={true}
                  fieldName="Password"
                  isError={errorHandlers.password}
                />
                {
                  errorHandlers.password && <Text style={styles.textError}>Password is required.</Text>
                }
                <Spacer space={5} />
                <TextInputField
                  maxLength={26}
                  keyboardType="default"
                  textInputChange={this.textInputOnChange}
                  iconColor={COLOR.GRAY}
                  iconSize={30}
                  iconName="lock-check"
                  placeholder="Confirm Password"
                  isSecure={true}
                  fieldName="ConfirmPassword"
                  isError={errorHandlers.confirmPassword}
                />
                {
                  errorHandlers.confirmPassword ?
                    <Text style={styles.textError}>Confirm Password is required.</Text>
                    : errorHandlers.passwordNotTheSame && <Text style={styles.textError}>Password did not Match.</Text>
                }
                <Spacer space={5} />
                <TouchableOpacity onPress={() => this.registerOnClick()}>
                  <Text style={styles.signUpBtn}>Sign Up</Text>
                </TouchableOpacity>
                <Spacer space={15} />
                <View>
                  <View
                    style={{
                      backgroundColor: COLOR.GRAY,
                      width: "80%",
                      alignSelf: "center",
                      height: 1,
                    }}
                  />
                  <Text
                    style={{
                      backgroundColor: COLOR.WHITE,
                      position: "absolute",
                      textAlign: "center",
                      top: -10,
                      paddingHorizontal: 10,
                      alignSelf: "center",
                    }}
                  >
                    or
                  </Text>
                </View>
                <Spacer space={10} />
                <TouchableOpacity onPress={() => console.log("Login")}>
                  <View style={{ flexDirection: "row" }}>
                    <Image
                      source={require("../assets/images/google.png")}
                      style={{
                        width: 30,
                        height: 30,
                        zIndex: 999,
                        top: 10,
                        position: "absolute",
                        left: 40,
                      }}
                    />
                    <Text
                      style={[
                        styles.buttons,
                        {
                          backgroundColor: COLOR.LIGHT_GRAY,
                          fontFamily: FONT.bold,
                          color: COLOR.BLACK,
                        },
                      ]}
                    >
                      SIGN UP WITH GOOGLE
                    </Text>
                  </View>
                </TouchableOpacity>
                <Spacer space={5} />
                <TouchableOpacity onPress={() => console.log("Login")}>
                  <View style={{ flexDirection: "row" }}>
                    <Image
                      source={require("../assets/images/facebook.png")}
                      style={{
                        width: 30,
                        height: 30,
                        zIndex: 999,
                        top: 10,
                        position: "absolute",
                        left: 40,
                      }}
                    />
                    <Text
                      style={[
                        styles.buttons,
                        {
                          backgroundColor: COLOR.FACEBOOK,
                          fontFamily: FONT.bold,
                          color: COLOR.WHITE,
                        },
                      ]}
                    >
                      SIGN UP WITH GOOGLE
                    </Text>
                  </View>
                </TouchableOpacity>
                <Spacer space={5} />
                <Text
                  style={{
                    textAlign: "center",
                    fontSize: 12,
                  }}
                >
                  By signing up, you agree to Humble Bundle's Terms & Conditions and
                  Privacy Policy.
                </Text>
                <Spacer space={15} />
                <TouchableOpacity onPress={() => Navigation.navigate("Login", {})}>
                  <Text
                    style={{
                      textAlign: "center",
                      fontFamily: FONT.bold,
                      fontSize: 15,
                      color: COLOR.HUMBLE_GRAY,
                    }}
                  >
                    Already have an account?{" "}
                    <Text
                      style={{
                        textDecorationLine: "underline",
                      }}
                    >
                      Login
                    </Text>
                  </Text>
                </TouchableOpacity>
                <Spacer space={10} />
              </View>
              <HumbleFooter />
            </ScrollView>
          </KeyboardAvoidingView>
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: COLOR.CUSTOM_GRAY,
  },
  createHumbleAcc: {
    fontFamily: FONT.bold,
    fontSize: 20,
    marginTop: 10,
  },
  buttons: {
    alignSelf: "center",
    color: COLOR.WHITE,
    paddingVertical: 15,
    width: "100%",
    textAlign: "center",
    backgroundColor: COLOR.BLUE,
    fontFamily: FONT.bold
  },
  signUpBtn: {
    alignSelf: "center",
    color: COLOR.WHITE,
    paddingVertical: 15,
    width: "100%",
    textAlign: "center",
    backgroundColor: COLOR.BLUE,
    fontFamily: FONT.bold,
  },
  containerBox: {
    backgroundColor: COLOR.WHITE,
    width: width * 0.95,
    marginVertical: 10,
    alignSelf: 'center',
    borderColor: COLOR.BORDER_COLOR,
    borderWidth: 1.5,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  textError: {
    fontSize: 12,
    color: COLOR.RED,
    paddingLeft: 10
  }
});

const mapStateToProps = (state: any) => ({
  error: state.authReducer.error,
  isLoading: state.authReducer.isLoading,
  success: state.authReducer.success
});

const mapDispatchToProps = (dispatch: any) => {
  return {
    register: (credentials: any) => {
      dispatch({
        type: types.REGISTER_REQUEST,
        payload: credentials
      })
    }
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
