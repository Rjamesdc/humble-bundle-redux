import React, { Component } from "react";
import {
  ScrollView,
  Animated,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
  Image,
  ImageBackground,
  StatusBar,
} from "react-native";
import { connect } from "react-redux";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import IconButton from "../components/IconButton";
import { COLOR, FONT, SIZES } from "../constants";
import * as Navigation from "../navigation/RootNavigation";
import LinearGradient from "react-native-linear-gradient";
const AnimatedLinearGradient = Animated.createAnimatedComponent(LinearGradient);

import { Games } from "../util/dummyData";

interface Props {
  userInfo: {
    background: "string";
    image: string;
    fullname: string;
  };
  onScroll: () => void;
  isLoading: boolean;
  toValue: number;
  duration: number;
}

interface State {
  offSet: boolean;
  fadeAnimation: any;
}
export class Profile extends Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      offSet: false,
      fadeAnimation: new Animated.Value(0),
    };
  }

  fadeIn = () => {
    Animated.timing(this.state.fadeAnimation, {
      toValue: 10,
      duration: 1000,
      useNativeDriver: true,
    }).start();
    this.setState({ offSet: true });
  };

  fadeOut = () => {
    Animated.timing(this.state.fadeAnimation, {
      toValue: 0,
      duration: 500,
      useNativeDriver: true,
    }).start();
  };
  onScroll = (e: any) => {
    if (e.nativeEvent.contentOffset.y > 310) {
      this.fadeIn();
    }
    if (e.nativeEvent.contentOffset.y <= 380) {
      this.fadeOut();
    }
  };

  profileHeader = () => {
    const { userInfo, isLoading } = this.props;
    return (
      <>
        <ImageBackground
          resizeMode="cover"
          style={{
            width: SIZES.width,
            height: 350,
          }}
          source={{
            uri: userInfo.background
          }}
        >
          <LinearGradient
            colors={[COLOR.TRANSPARENT, COLOR.HEADER_BACKGROUND]}
            style={{
              height: 1000,
              width: SIZES.width,
              position: "absolute",
              bottom: 0,
            }}
          />
          <View style={{ alignItems: "center", top: "35%" }}>
            <Image
              style={{
                width: 120,
                height: 120,
                borderRadius: 100,
                borderWidth: 5,
                borderColor: COLOR.HEADER_BACKGROUND,
                zIndex: 100,
              }}
              resizeMode="cover"
              source={{
                uri: userInfo.image,
              }}
            />
            <Text
              style={{
                marginTop: 10,
                fontFamily: FONT.bold,
                fontSize: 20,
                color: COLOR.WHITE,
              }}
            >
              {userInfo.fullname}
            </Text>
          </View>
        </ImageBackground>
        <View
          style={{
            top: -30,
            paddingHorizontal: 20,
            flexDirection: "row",
            justifyContent: "space-around",
          }}
        >
          <Text
            style={{
              color: COLOR.WHITE,
              fontFamily: FONT.bold,
              textAlign: "center",
              fontSize: 20,
            }}
          >
            93{"\n"}
            <Text
              style={{
                fontSize: 12,
              }}
            >
              Games Owned
            </Text>
          </Text>
          <Text
            style={{
              color: COLOR.WHITE,
              fontFamily: FONT.bold,
              textAlign: "center",
              fontSize: 20,
            }}
          >
            3,321{"\n"}
            <Text
              style={{
                fontSize: 12,
              }}
            >
              Followers
            </Text>
          </Text>
          <Text
            style={{
              color: COLOR.WHITE,
              fontFamily: FONT.bold,
              textAlign: "center",
              fontSize: 20,
            }}
          >
            1,293{"\n"}
            <Text
              style={{
                fontSize: 12,
              }}
            >
              Reputation
            </Text>
          </Text>
        </View>
      </>
    );
  };

  render() {
    const { userInfo, isLoading } = this.props;
    return (
      <>
        <StatusBar translucent={true} backgroundColor={"transparent"} />
        <View
          style={{
            position: "absolute",
            zIndex: 999,
          }}
        >
          <AnimatedLinearGradient
            colors={[
              COLOR.HEADER_BACKGROUND,
              COLOR.HEADER_BACKGROUND,
              COLOR.TRANSPARENT,
            ]}
            style={{
              opacity: this.state.fadeAnimation,
              height: SIZES.height / 5,
              width: SIZES.width,
            }}
          />
          <IconButton
            customStyle={{
              position: "absolute",
              top: SIZES.height / 12,
              left: 25,
            }}
            color={COLOR.WHITE}
            iconPress={() => Navigation.openDrawer()}
            size={25}
            name="menu"
          />
          <IconButton
            customStyle={{
              position: "absolute",
              top: SIZES.height / 13,
              right: 20,
            }}
            color={COLOR.WHITE}
            iconPress={() => Navigation.navigate("UpdateProfile", {})}
            size={35}
            name="pencil"
          />
          <IconButton
            customStyle={{
              position: "absolute",
              top: SIZES.height / 13,
              right: 70,
            }}
            color={COLOR.WHITE}
            iconPress={() => console.log("mymessages")}
            size={35}
            name="message-bulleted"
          />
        </View>
        <View style={styles.container}>
          <FlatList
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={this.profileHeader}
            onScroll={this.onScroll}
            data={[{}]}
            keyExtractor={(item, index): any => index + 1}
            renderItem={({ item, index }: any) => (
              <View
                key={index + 1}
                style={{
                  paddingHorizontal: 5,
                }}
              >
                <View 
                style={{ paddingHorizontal: 5 }}>
                  <Text
                    style={{
                      fontFamily: FONT.bold,
                      fontSize: 15,
                      color: COLOR.WHITE,
                      marginBottom: 20,
                    }}
                  >
                    Badges Showcase
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                    }}
                  >
                    <Icon
                      color={COLOR.YELLOW}
                      size={45}
                      name="zigbee"
                      style={{ paddingRight: 5 }}
                    />
                    <Icon
                      color={COLOR.WHITE}
                      size={45}
                      name="yin-yang"
                      style={{ paddingRight: 5 }}
                    />
                    <Icon
                      color={COLOR.RED}
                      size={45}
                      name="youtube-gaming"
                      style={{ paddingRight: 5 }}
                    />
                    <Icon
                      color={COLOR.BLUE}
                      size={45}
                      name="zend"
                      style={{ paddingRight: 5 }}
                    />
                  </View>
                </View>

                <Text
                  style={{
                    marginLeft: 5,
                    marginTop: 20,
                    fontFamily: FONT.bold,
                    fontSize: 15,
                    color: COLOR.WHITE,
                  }}
                >
                  Recently Played
                </Text>
                <FlatList
                  data={Games}
                  horizontal
                  showsHorizontalScrollIndicator={false}
                  keyExtractor={(item: any, index:any) => item.id + 1 + index}
                  renderItem={({ item, index }): any => (
                    <View
                      key={item.id + index}
                      style={{
                        paddingHorizontal: 5,
                      }}
                    >
                      <Image
                        resizeMode="contain"
                        style={{
                          width: 130,
                          height: 100,
                        }}
                        source={{ uri: item.image }}
                      />
                    </View>
                  )}
                />
              </View>
            )}
          />
        </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: COLOR.HEADER_BACKGROUND,
  },
  header: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  profileCard: {
    marginTop: 10,
  },
  profileDetails: {
    paddingHorizontal: 20,
    bottom: 80,
  },
});

const mapStateToProps = (state: any) => ({
  userInfo: state.authReducer.userInfo,
  isLoading: state.authReducer.isLoading,
});

const mapDispatchToProps = (dispatch: any) => {
  return {};
};
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
