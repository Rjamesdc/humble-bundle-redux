import Login from './Login';
import HumbleHome from './HumbleHome';
import SignUp from './SignUp';
import Profile from './Profile';
import UpdateProfile from './UpdateProfile';

export {
    //Authentications
    Login,
    SignUp,

    //Mains
    HumbleHome,
    Profile,

    //Sub Pages
    UpdateProfile
}