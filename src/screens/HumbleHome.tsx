import React, { Component } from 'react'
import { Text, View, StyleSheet, Dimensions, TextInput, Image, ScrollView, FlatList, StatusBar } from 'react-native'
import FONT from '../constants/fonts';
import COLOR from '../constants/colors';
import HumbleHeader from '../components/HumbleHeader';
import LinearGradient from 'react-native-linear-gradient';
import Spacer from '../components/Spacer';
const { width, height } = Dimensions.get("window");
import GameCards from '../components/GameCards';
import { connect } from 'react-redux';
import { types } from '../redux/Games/types'
import FocusAwareStatusBar from '../navigation/StatusBarUtil';

interface Props{
    userInfo: {},
    getGames: () => void,
    games: []
    navigation: any
}

interface State {
    gameLists: []
}

export class HumbleHome extends Component<Props, State> {    
    constructor(props: Props){
        super(props);
        this.state = {
            gameLists: []
        }
    }

    componentDidMount(){
        this.getAllGameLists()
    }

    getAllGameLists = async () => {
        const { getGames } = this.props;
        getGames()
    }

    renderItem = ({ item }:any) =>{
        return(
            <GameCards item={item}/>
        )
    }

    membership = ():any => {
        return(
            <View style={{
                paddingHorizontal: 20
            }}>
            <Text style={{
                color: COLOR.WHITE,
                fontFamily: FONT.bold,
                fontSize: 15,
                marginTop: 20
            }}> Humble Choice Membership </Text>
        <Spacer space={10}/>
        <LinearGradient
            colors={[COLOR.BLACK, COLOR.HUMBLE_GRAY]}
            style={{
                width: '100%',
                paddingVertical: 25,
                paddingHorizontal: 20,
                alignItems: 'center',
                borderRadius: 10
            }}
          >
              <Text style={{
                  color: COLOR.WHITE,
                  width: '79%',
                  textAlign: 'center',
                  fontFamily: FONT.bold,
                  fontSize: 20
              }}>
                  Join the humble choice and get 12 Games for only $12!
              </Text>
              <Spacer space={15}/>
              <View>
              <Text style={styles.playnowCaret}>
                  START PLAYING NOW
              </Text>
            <Image source={require('../assets/images/productholder.png')}
                style={{
                    borderWidth: 5,
                    borderColor: COLOR.WHITE,
                    borderRadius: 10
                }}
            />
            </View>
            <Spacer space={10}/>
            <LinearGradient
            colors={[COLOR.HUMBLE_YELLOW, COLOR.HUMBLE_ORANGE]}
            // start={{ x: 1, y: 0 }}
            // end={{ x: 0, y: 1 }}
            style={{
                width: '95%',
                paddingVertical: 25,
                paddingHorizontal: 20,
                alignItems: 'center',
                borderRadius: 10,
                elevation: 10
            }}
          >
              <Text style={{
                  color: COLOR.WHITE,
                  fontFamily: FONT.bold
              }}>Join Now</Text>
        </LinearGradient>
        </LinearGradient>
        </View>
        )
    }

    render() {
        const { games } = this.props;
        return (
            <>
            <FocusAwareStatusBar barStyle="light-content" translucent={true} backgroundColor={COLOR.TRANSPARENT}/>
            <HumbleHeader backBtn={false} title="Humble" showMenu={true}/>
            <View style={styles.container}>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={games}
                keyExtractor={(item:any) => item.id + 1}
                renderItem={this.renderItem}
                ListHeaderComponent={this.membership}
                />
            </View>
            </>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#171717",
    },
    playnowCaret: {
        color: COLOR.BLACK,
        fontFamily: FONT.bold,
        backgroundColor: COLOR.WHITE,
        paddingHorizontal:  10,
        paddingVertical: 5,
        borderRadius: 5,
        position: 'absolute',
        zIndex: 999,
        top: -10,
        alignSelf: 'center'
    }
})

const mapStateToProps = (state: any) => ({
    games: state.gamesReducer.games,
    error: state.gamesReducer.error,
    isLoading: state.gamesReducer.isLoading,
  });
  
  const mapDispatchToProps = (dispatch: any) => {
    return {
      getGames: () => {
          dispatch({
              type: types.GET_GAMES
          })
      }
    };
  };

export default connect(mapStateToProps, mapDispatchToProps)(HumbleHome);
// export default connect(mapStateToProps, mapDispatchToProps)(Login);
