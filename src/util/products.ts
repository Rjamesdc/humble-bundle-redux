export default [
    { id: 1, title: 'iPad 4 Mini', price: 500.01, inventory: 2 },
    { id: 2, title: 'H&M T-Shirt White', price: 10.99, inventory: 10 },
    { id: 3, title: 'iPad 5', price: 400.51, inventory: 1 },
    { id: 4, title: 'Huawei', price: 550.31, inventory: 3 },
    { id: 5, title: 'Iphone 13', price: 350.21, inventory: 9 },
    { id: 6, title: 'Neck-Deep LNOTGY', price: 250.91, inventory: 8 },
    { id: 7, title: 'Jeremy Zucker - Clouds', price: 100.61, inventory: 7 },
    { id: 8, title: 'Charli XCX - Sucker CD', price: 19.99, inventory: 5 },
  ]
  