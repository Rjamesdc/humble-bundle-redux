export const Games = [
    {
     id: 1,
     title: 'Death Loop',
     price: 'P3,014.00',
     image: 'https://hb.imgix.net/c5053d8239862b84a3830c437d145df0c3f60d46.jpg?auto=compress,format&fit=crop&h=353&w=616&s=f6392afda0aaa05c63c612b0bf48a1ea'
    },
    {
     id: 2,
     title: 'Valheim',
     price: 'P520.00',
     image: 'https://hb.imgix.net/f628cdf98b24e896de36a11a838040639dc276f5.jpg?auto=compress,format&fit=crop&h=353&w=616&s=16f4ada2b70f73282cbdb0b57b51c02f'
    },
    {
     id: 3,
     title: 'Satisfactory',
     price: 'P3,400.00',
     image: 'https://hb.imgix.net/3408de5131f62c11c1c4936975c195a6753fccea.jpeg?auto=compress,format&fit=crop&h=353&w=616&s=a243a8ffdaed5073b160dcd280f13c91'
    },
    {
    id: 4,
    title: 'World War Z AfterMath',
    price: 'P2,512.00',
    image: 'https://hb.imgix.net/f85dc009c1225ad57293e7624d0d8ccba678142e.jpg?auto=compress,format&fit=crop&h=353&w=616&s=1eaf103b0b60641f8139be3ee21527f2'
    }
];