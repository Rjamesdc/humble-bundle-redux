import React, { Component } from 'react'
import { Text, View, TextInput, StyleSheet, KeyboardTypeOptions } from 'react-native'
import COLOR from '../constants/colors'
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import VerticalSeparator from './VerticalSeparator';


interface TextInputFieldProps { 
    iconName: string,
    keyboardType?: KeyboardTypeOptions,
    placeholder: string,
    iconColor: string,
    iconSize: number,
    textInputChange: (text:string, fieldName: string) => void,
    isSecure?: boolean
    fieldName: string
    textContentType?: any
    isError?: boolean
    maxLength?: number
}

export class TextInputField extends Component<TextInputFieldProps>{
    render() {
        const { iconName, keyboardType, placeholder, iconColor, iconSize, isSecure, textInputChange, fieldName, textContentType, isError, maxLength } = this.props;
        return (
            <View style={[styles.inputBox, { borderColor: isError ? COLOR.RED : COLOR.BORDER_COLOR}]}>
            <Icon
              name={iconName}
              style={styles.iconStyle}
              size={iconSize}
              color={iconColor}
            />
            <VerticalSeparator spacing={5} size={30} />
            <TextInput
              maxLength={maxLength}
              textContentType={textContentType}
              returnKeyType='next'
              secureTextEntry={isSecure}
              keyboardType={keyboardType}
              placeholder={placeholder}
              style={styles.textInputs}
              onChangeText={(text) => {
                  textInputChange(text,fieldName)
              }}
            />
          </View>
        )
    }
}

const styles = StyleSheet.create({
    inputBox: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,
        paddingVertical: 2,
        borderWidth: 1.5,
    },
    iconStyle:{
        alignItems: "center",
    },
    textInputs:{
        width: "85%", 
        height: "80%", 
        paddingLeft: 10,
        borderColor: COLOR.RED
    }
})

export default TextInputField
