import React, { Component } from 'react'
import { Text, View } from 'react-native'

interface Props {
    space: number
}

export class Spacer extends Component<Props> {
    render() {
        const { space } = this.props;
        return (
            <View style={{
                paddingVertical: space
            }}/>
        )
    }
}

export default Spacer
