import React, { Component } from 'react'
import { Text, View, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native'
import COLORS from '../constants/colors';
import FONT from '../constants/fonts';
const { width, height } = Dimensions.get("window");
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

interface Props {
    customAction: any
}

export class CustomBackButton extends Component<Props> {
    render() {
        const { customAction } = this.props;
        return (
            <View style={styles.container}>
                    <TouchableOpacity style={{
                        position: 'absolute',
                        top:55,
                        left: 25,
                        zIndex: 5
                    }}
                    onPress={() => customAction()}
                    >
                    <Icon
                        name="arrow-left-circle"
                        size={30}
                        color={COLORS.WHITE}
                    />
                    </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLORS.HEADER_BACKGROUND,
        height: height / 7,
        width
    },
    humbleTitle: {
        fontFamily: FONT.cursive,
        color: COLORS.WHITE,
        fontSize: 20,
    }
})

export default CustomBackButton
