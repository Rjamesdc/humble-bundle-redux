import React, { Component } from 'react'
import { View, TouchableOpacity } from 'react-native'
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

interface Props{
    iconPress?: any;
    size: number;
    color: string;
    name: string;
    customStyle?: any
} 

export class IconButton extends Component<Props>{
    render() {
        const { iconPress, size, color, name, customStyle } = this.props;
        return (
            <View style={customStyle}>
               <TouchableOpacity onPress={() => iconPress()}>
                    <Icon
                        name={name}
                        size={size}
                        color={color}
                    />
                    </TouchableOpacity>
            </View>
        )
    }
}

export default IconButton
