import React, { Component } from 'react'
import { Text, View, StyleSheet, Image} from 'react-native'
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import COLOR from '../constants/colors';
import FONT from '../constants/fonts';

interface Props {
    item: {
        title: string,
        image: string,
        price: string,
        platform: any
    }
}

export class GameCards extends Component<Props> {
    render() {
        const { item } = this.props;
        const platforms = item.platform.split(',')
        return (
            <>
            <View style={styles.container}>
            <Image 
            resizeMode='contain'
            source={{
             uri: item.image,
             }} style={{
                 width: '100%',
                 height: 210
             }}/>
             <View style={{
                 backgroundColor: COLOR.WHITE,
                 paddingVertical: 20,
                 paddingHorizontal: 10,
                 top: -5,
                 flexDirection: 'row',
                 justifyContent: 'space-between'
             }}>
                <Text style={{
                    fontFamily: FONT.bold,
                }}> {item.title} </Text>
                <View style={{
                    position: 'absolute',
                    flexDirection: 'row',
                    top: 20,
                    right: item.price.length < 8 ? 70 : 85
                }}>
                    {platforms.map((x:any, index:number) => 
                        <Icon
                        key={index + 1}
                        name={x}
                        size={20}
                        color={COLOR.GRAY}
                      />
                    )}
                </View>
                <Text style={{
                    fontFamily: FONT.bold,
                }}>
                    {`P${item.price}`}
                </Text>
             </View>
            </View>
            </>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 30,
        paddingVertical: 10
    }
})

export default GameCards
