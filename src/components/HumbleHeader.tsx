import React, { Component } from 'react'
import { Text, View, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native'
import COLORS from '../constants/colors';
import FONT from '../constants/fonts';
const { width, height } = Dimensions.get("window");
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import * as Navigation from '../navigation/RootNavigation'

interface Props {
    title: string;
    showMenu: boolean;
    backBtn: boolean;
}

export class HumbleHeader extends Component<Props> {
    render() {
        const { title, showMenu, backBtn } = this.props
        return (
            <View style={styles.container}>
                {
                    backBtn && 
                    <TouchableOpacity style={{
                        position: 'absolute',
                        top:55,
                        left: 25,
                        zIndex: 5
                    }}
                    onPress={() => Navigation.goBack()}
                    >
                    <Icon
                        name="arrow-left-circle"
                        size={30}
                        color={COLORS.WHITE}
                    />
                    </TouchableOpacity>
                }
                   {showMenu
                    &&
                    <TouchableOpacity style={{
                        position: 'absolute',
                        top:55,
                        left: 25,
                        zIndex: 5
                    }} onPress={()=> Navigation.openDrawer()}>
                        <Image source={require('../assets/images/icon_menu.png')}
                        style={{
                            width: 25,
                            height: 25,
                            tintColor: 'white',
                        }}
                        />
                    </TouchableOpacity>
                }
                <View style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent:'center',
                    flex: 1,
                    top: 15
                }}>
                <Text style={styles.humbleTitle}> {title} </Text>
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: COLORS.HEADER_BACKGROUND,
        height: height / 7,
        width
    },
    humbleTitle: {
        fontFamily: FONT.cursive,
        color: COLORS.WHITE,
        fontSize: 20,
    }
})

export default HumbleHeader
