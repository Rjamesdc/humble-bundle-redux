import React, { Component } from 'react'
import { Text, View, StyleSheet } from 'react-native'
import COLORS from '../constants/colors';

interface Props {
    spacing: number,
    size: number
}

export class VerticalSeparator extends Component<Props> {
    render() {
        const { spacing, size } = this.props;
        return (
            <View style={{
                paddingHorizontal: spacing
            }}>
                <View 
                style={{
                    paddingHorizontal: 1,
                    backgroundColor: COLORS.GRAY,
                    width: 1,
                    height: size
                }}/>
            </View>

        )
    }
}

export default VerticalSeparator
