import React, { Component } from 'react'
import { Text, View, StyleSheet, Dimensions } from 'react-native'
import COLOR from '../constants/colors'
const { width, height } = Dimensions.get("window");
import FONT from '../constants/fonts'
import Spacer from './Spacer';


export class HumbleFooter extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={{
                    fontFamily: FONT.cursive,
                    color: COLOR.WHITE,
                    fontSize: 17
                }}> Humble </Text>
                <Spacer space={5}/>
                <View style={{
                    paddingHorizontal: 5
                }}>
                    <Text style={{
                        fontSize: 12,
                        color: COLOR.WHITE
                    }}>Get up to P750 Wallet Credit to spend on your next store purchase!</Text>
                    <Spacer space={8}/>
                    <Text style={{
                        fontSize: 15,
                        fontFamily: FONT.bold,
                        color: COLOR.INVITE
                    }}>
                        Invite A Friend
                    </Text>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: COLOR.HEADER_BACKGROUND,
        width: width,
        paddingHorizontal: 20,
        paddingVertical: 20
    }
})

export default HumbleFooter
