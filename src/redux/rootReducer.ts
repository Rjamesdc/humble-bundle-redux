import { combineReducers } from "redux";
import authReducer from "./Auth/reducer";
import gamesReducer from "./Games/reducer";

const rootReducer = combineReducers({
    authReducer,
    gamesReducer
})

export default rootReducer;
