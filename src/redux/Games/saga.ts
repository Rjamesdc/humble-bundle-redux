import { takeLatest, put, call, delay } from "@redux-saga/core/effects";
import { types } from "./types";
import axios from "axios";
import { asyncGetGames } from "./effects";
import * as NavigationService from '../../navigation/RootNavigation';
import AsyncStorage from '@react-native-async-storage/async-storage';

export interface RequestResponse {
    data: any,
    status: number,
    page?: number,
    perPage?: number, 
    length?: number | undefined,
    message?: any
  }
  
  function* getGames(action:any) {
    const games:RequestResponse = yield call(asyncGetGames, {});
      if(games.status === 200){
        yield put({
          type: types.GET_GAMES_SUCCESS,
          payload: games.data
        })
      }else{
        yield put({
          type: types.GET_GAMES_FAILED,
          payload: games
        })
      }
  }  
  
  const gamesSaga = [
    takeLatest(types.GET_GAMES, getGames),
    // takeLatest(types.REMOVE_BOOK, removeBook)
    // ...takeLatest 
  ];
  
  export default gamesSaga;
  