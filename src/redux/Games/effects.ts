import { types } from "./types";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

export const asyncGetGames = async (params:any) => {
     try {
        const gameDatas: any = await axios.get(`http://10.0.2.2:3333/api/v1/games`);
        return gameDatas
     } catch (error:any) {
         const errors = {
             status: error.response.status,
             message: error.response.data.data
         }
         return errors
     }
}


