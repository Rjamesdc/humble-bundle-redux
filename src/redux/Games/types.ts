export const types = {
    GET_GAMES: 'GET_GAMES',
    GET_GAMES_SUCCESS: 'GET_GAMES_SUCCESS',
    GET_GAMES_FAILED: 'GET_GAMES_FAILED'
}