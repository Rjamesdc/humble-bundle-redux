import { types } from './types';

interface BookTypes {
    isLoading: boolean;
    error: {};
    games: any
}

const initialState:BookTypes = {
    isLoading: false,
    error: {},
    games: []
}

const gamesReducer = (state = initialState, action:any) => {
    switch (action.type) {
        case types.GET_GAMES:
            return{
                ...state,
                isLoading: true,
            }
        case types.GET_GAMES_SUCCESS:
            return{
                ...state,
                isLoading: false,
                games: action.payload,
                error: {}
            }
        case types.GET_GAMES_FAILED:
            return{
                ...state,
                isLoading:false,
                error: action.payload,
                games: []
            }
        default:
            return state;
    }
}


export default gamesReducer;