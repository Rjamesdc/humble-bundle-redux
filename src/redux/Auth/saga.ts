import { takeLatest, put, call, delay } from "@redux-saga/core/effects";
import { types } from "./types";
import { asyncLogin, asyncLogout, asyncRegister, asyncUpdateProfile } from "./effects";
import * as Navigation from '../../navigation/RootNavigation';
import AsyncStorage from '@react-native-async-storage/async-storage';

export interface RequestResponse {
    data: any,
    status: number,
    page?: number,
    perPage?: number, 
    length?: number | undefined,
    message?: any
  }
  
  function* login(action:any) {
    const auth:RequestResponse = yield call(asyncLogin, action.payload);
    if(auth.status === 200){
      yield call(AsyncStorage.setItem, 'userDetails', JSON.stringify(auth.data.user))
      yield put({
        type: types.GET_USER_SUCCESS,
        payload: auth.data.user
      })
      // Note that home is the name of Drawer Navigation not the actual Humble Home
      Navigation.navigate('Home', {})
    }else{
      yield put({
        type: types.GET_USER_FAILED,
        payload: auth
      })
    }
  }  

  function* register(action:any){
    const register:RequestResponse = yield call(asyncRegister, action.payload);
    if(register.status === 200){
      yield put({
        type: types.REGISTER_REQUEST_SUCCESS,
        payload: register.data.user
      })
      Navigation.navigate('Login', {})
    }
    else{
      yield put({
        type: types.REGISTER_REQUEST_FAIELD,
        payload: register
      })
    }
  }
  
  function* logout(action:any){
    const clearUser:RequestResponse = yield call(asyncLogout);
      if(clearUser){
        yield put({
          type: types.LOGOUT_REQUEST_SUCCESS,
          payload: {
            status: 200,
            success: true,
            message: 'Logout success'
          }
        })
        Navigation.navigate('Login', {})
      }else{
        yield put({
          type: types.LOGOUT_REQUEST_FAILED,
          payload: {
            status: 417,
            message: 'Expectaion failed, unable to logout',
            success: false
          }
        })
      }
  }

  function* updateProfile(action:any){
    const updateUser:RequestResponse = yield call(asyncUpdateProfile, action.payload);
      if(updateUser.status === 200){
        yield put({
          type: types.UPDATE_PROFILE_SUCCESS,
          payload: updateUser
        })
      }else{
        console.log(updateUser)
        yield put({
          type: types.UPDATE_PROFILE_FAILED,
          payload: updateUser
        })
      }
  }

  const authSaga = [
    takeLatest(types.GET_USER, login),
    takeLatest(types.REGISTER_REQUEST, register),
    takeLatest(types.LOGOUT_REQUEST, logout),
    takeLatest(types.UPDATE_PROFILE, updateProfile),
    // takeLatest(types.REMOVE_BOOK, removeBook)
    // ...takeLatest 
  ];
  
  export default authSaga;
  