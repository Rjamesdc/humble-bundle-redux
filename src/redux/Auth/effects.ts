import { types } from "./types";
import axios from "axios";
import AsyncStorage from "@react-native-async-storage/async-storage";

export interface RequestResponse {
    data: any,
    status: number,
    page?: number,
    perPage?: number,
    length?: number | undefined,
  }

export const asyncLogin = async (params:any) => {
     try {
        const checkUser: RequestResponse = await axios.post(`http://10.0.2.2:3333/api/v1/login`, params);
        return checkUser
     } catch (error:any) {
         const errors = {
             status: error.response.status,
             message: error.response.data.data
         }
         return errors
     }
}

export const asyncRegister = async (params: any) => {
    try{
        const response: RequestResponse = await axios.post('http://10.0.2.2:3333/api/v1/signup', params); 
        return response
    }catch(err:any){
        const errors = {
            status: err.response.status,
            message: err.response.data.data
        }
        return errors
    }
}

export const asyncLogout = async () => {
    try{
        await AsyncStorage.removeItem('userDetails')
        return true;
    }catch(err: any){
        console.log(err)
    }
}

export const asyncUpdateProfile = async (params:any) => {
    try{
        const response:RequestResponse = await axios.put('http://10.0.2.2:3333/api/v1/updateprofile', params)
        return response
    }catch(err:any){
        return err.response
    }
}


