import { types } from "./types";

interface AuthTypes {
    isLoading: boolean,
    userInfo: {},
    success: {},
    error: {}
}

const initialState:AuthTypes = {
    isLoading: false,
    userInfo: {},
    success: {},
    error: {}
}

const authReducer = (state = initialState, action:any) => {
    switch (action.type) {
        case types.GET_USER:
            return{
                ...state,
                isLoading: true,
            }
        case types.GET_USER_SUCCESS:
        return{
            ...state,
            isLoading: false,
            userInfo: action.payload,
            error: {}
        }
        case types.GET_USER_FAILED:
            return{
                ...state,
                isLoading: false,
                userInfo: {},
                error: action.payload
            }
        case types.REGISTER_REQUEST:
            return{
                ...state,
                isLoading: true
            }
        case types.REGISTER_REQUEST_SUCCESS:
            return{
                ...state,
                isLoading: false,
                success: action.payload,
                error: {}
            }
        case types.REGISTER_REQUEST_FAIELD:
            return{
                ...state,
                isLoading: false,
                success: {},
                error: action.payload
            }
        case types.LOGOUT_REQUEST: 
            return{
                ...state,
                isLoading: true
            }
        case types.LOGOUT_REQUEST_SUCCESS:
            return{
                ...state,
                userInfo: {},
                isLoading: false,
                success: action.payload,
                error: {}
            }
        case types.LOGOUT_REQUEST_FAILED:
            return{
                ...state,
                success: {},
                error: action.payload,
                isLoading: false
            }
        case types.UPDATE_PROFILE: 
            return{
                ...state,
                isLoading: true,
            }
        case types.UPDATE_PROFILE_SUCCESS:
            return{
                ...state,
                isLoading: false,
                userInfo: action.payload.data,
                success: action.payload,
                error: {}
            }
        case types.UPDATE_PROFILE_FAILED:
            return{
                ...state,
                isLoading: false,
                userInfo: state.userInfo,
                error: action.payload,
                success: {}
            }
        case types.UPDATE_PROFILE_CLEAR:
            return{
                ...state,
                isLoading: false,
                error: {},
                success: {}
            }
        default:
            return state;
    }
}


export default authReducer;