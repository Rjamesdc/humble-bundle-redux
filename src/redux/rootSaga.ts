import {take, call, put, all} from "@redux-saga/core/effects";
import authSaga from "./Auth/saga";
import gamesSaga from "./Games/saga";

export default function* RootSaga(){
    yield all([
        ...authSaga,
        ...gamesSaga
    ])
}
