import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  Image,
  ImageBackground,
  Dimensions,
  StyleSheet,
} from "react-native";
import { DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { connect } from "react-redux";
import COLOR from "../constants/colors";
import FONT from "../constants/fonts";
import { types } from "../redux/Auth/types";
const { width, height } = Dimensions.get("window");
import LinearGradient from 'react-native-linear-gradient';
import * as Navigation from '../navigation/RootNavigation'

interface DrawerProps {
    logout: any;
    error: {}
    success: {}
    isLoading: boolean
    userInfo: any
}
export class DrawerContent extends Component<DrawerProps> {
  render() {
    const { logout, error, success, isLoading, userInfo } = this.props;
    return (
      <>
      <View style={styles.drawerContainer}>
        <DrawerContentScrollView {...this.props}>
          <View>
            <ImageBackground
              imageStyle={{
                opacity: 0.85,
              }}
              resizeMode="cover"
              style={{
                width: 300,
                height: 100,
                top: -5,
              }}
              source={{
                uri: userInfo.background
              }}
            >
              <View
                style={{
                  top: 50,
                  left: 20,
                }}
              >
                <Image
                  style={{
                    width: 75,
                    height: 75,
                    borderRadius: 50,
                    borderWidth: 3,
                    borderColor: COLOR.HEADER_BACKGROUND,
                    zIndex:100
                  }}
                  resizeMode="cover"
                  source={{
                    uri: userInfo.image,
                  }}
                />
                <View style={{
                    position: 'absolute',
                    left: '30%',
                    top: '40%',
                    zIndex: 1
                }}>
                    <Text style={{
                        fontFamily: FONT.bold,
                        color: COLOR.WHITE,
                        fontSize: 15,
                        width: '80%',
                        marginBottom: 5
                    }}>{userInfo.fullname}</Text>
                    <Text style={{
                        fontFamily: FONT.regular,
                        color: COLOR.WHITE,
                        fontSize: 10,
                    }}>Busy</Text>
                </View>
                <View style={{
                    position: 'absolute',
                    top: '70%',
                    left: '15%',
                    zIndex: 999
                }}>
                 <Icon name="moon-waxing-crescent" color={COLOR.YELLOW} style={{
                    backgroundColor: COLOR.HEADER_BACKGROUND,
                    paddingVertical: 2.5,
                    paddingHorizontal: 2.5,
                    borderRadius: 20,
                    transform:[{rotateX:"5deg"},{rotateZ:"55deg"}]
                 }}size={20} />
                </View>
              </View>
              <LinearGradient
                    colors={[COLOR.TRANSPARENT, COLOR.HEADER_BACKGROUND, COLOR.HEADER_BACKGROUND]}
                    // start={{ x: 0, y: 1 }}
                    // end={{ x: 1, y: 0 }}
                    style={{
                        top: 50,
                        left: -100,
                        width: width,
                        height: 50,
                        borderRadius: 10,
                        position: 'absolute',
                    }}
                />
            </ImageBackground>
          </View>
          <View
            style={{
              marginTop: 30,
            }}
          >
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="home" color={COLOR.WHITE} size={size} />
              )}
              label="Home"
              labelStyle={{
                color: COLOR.WHITE,
                fontFamily: FONT.regular,
                fontSize: 15,
              }}
              onPress={() => Navigation.navigate('HumbleHome',{})}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="account" color={COLOR.WHITE} size={size} />
              )}
              label="Profile"
              labelStyle={{
                color: COLOR.WHITE,
                fontFamily: FONT.regular,
                fontSize: 15,
              }}
              onPress={() => Navigation.navigate('Profile',{})}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="book" color={COLOR.WHITE} size={size} />
              )}
              label="Library"
              labelStyle={{
                color: COLOR.WHITE,
                fontFamily: FONT.regular,
                fontSize: 15,
              }}
              onPress={() => {
                console.log("support");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="account-group" color={COLOR.WHITE} size={size} />
              )}
              label="You and Friends"
              labelStyle={{
                color: COLOR.WHITE,
                fontFamily: FONT.regular,
                fontSize: 15,
              }}
              onPress={() => {
                console.log("support");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="forum" color={COLOR.WHITE} size={size} />
              )}
              label="Community"
              labelStyle={{
                color: COLOR.WHITE,
                fontFamily: FONT.regular,
                fontSize: 15,
              }}
              onPress={() => {
                console.log("support");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="cog" color={COLOR.WHITE} size={size} />
              )}
              label="Settings"
              labelStyle={{
                color: COLOR.WHITE,
                fontFamily: FONT.regular,
                fontSize: 15,
              }}
              onPress={() => {
                console.log("support");
              }}
            />
            <DrawerItem
              icon={({ color, size }) => (
                <Icon name="face-agent" color={COLOR.WHITE} size={size} />
              )}
              label="Contact Support"
              labelStyle={{
                color: COLOR.WHITE,
                fontFamily: FONT.regular,
                fontSize: 15,
              }}
              onPress={() => {
                console.log("support");
              }}
            />
          </View>
        </DrawerContentScrollView>
        <View
          style={{
            bottom: 20,
          }}
        >
          <DrawerItem
            icon={({ color, size }) => (
              <Icon name="logout" color={COLOR.WHITE} size={size} />
            )}
            label="Log Out"
            labelStyle={{
              color: COLOR.RED,
              fontFamily: FONT.regular,
              fontSize: 15,
            }}
            onPress={() => logout()}
          />
        </View>
      </View>
      </>
    );
  }
}

const styles = StyleSheet.create({
  drawerContainer: {
    flex: 1,
    backgroundColor: COLOR.HEADER_BACKGROUND,
  },
});

const mapStateToProps = (state: any) => ({
    userInfo: state.authReducer.userInfo,
    isLoading: state.authReducer.isLoading,
    success: state.authReducer.success,
    error: state.authReducer.error,
});

const mapDispatchToProps = (dispatch: any) => {
    return {
      logout: () => {
       dispatch({
           type: types.LOGOUT_REQUEST
       })
      }
    };
  };
  

export default connect(mapStateToProps, mapDispatchToProps)(DrawerContent);
