import * as React from 'react';
import { DrawerActions } from '@react-navigation/routers';


export const navigationRef:any = React.createRef();

export function navigate(name:string, params:any) {
  navigationRef.current?.navigate(name, params);
}

export function goBack() {
  navigationRef.current?.goBack();
}

export function openDrawer() {
  navigationRef.current?.dispatch(DrawerActions.toggleDrawer())
}
