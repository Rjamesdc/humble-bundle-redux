import React, { FC } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator, } from '@react-navigation/drawer';
import COLORS from '../constants/colors'
import { 
  Login, 
  HumbleHome, 
  SignUp, 
  Profile,
  UpdateProfile
} from '../screens';
import DrawerContent from './DrawerContent';


const NavigationStack:any = createNativeStackNavigator();
const Drawer:any = createDrawerNavigator();


interface Props {
  title?: string;
  color?: string;
  style?: any;
}


function MyDrawer() {
  return (
    <Drawer.Navigator
      initialRouteName="HumbleHome"
      screenOptions={{
        headerShown: false
      }}
    drawerContent={props => <DrawerContent {...props} />}
    >
      <Drawer.Screen name="HumbleHome" component={HumbleHome} />
      <Drawer.Screen name="Profile" component={Profile} />
    </Drawer.Navigator>
  );
}

export const Main: FC<Props> = () => (
  <NavigationStack.Navigator
    screenOptions={{
      headerShown: false
    }}>
    <NavigationStack.Screen
      name='Login'
      component={Login}
    />
    <NavigationStack.Screen
      name="Home"
      component={MyDrawer}
    />
    <NavigationStack.Screen
      name="SignUp"
      component={SignUp}
    />
      <NavigationStack.Screen
      name="UpdateProfile"
      component={UpdateProfile}
    />
  </NavigationStack.Navigator>
);
