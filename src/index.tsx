import React from "react";
import { View, LogBox, StatusBar } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import "react-native-gesture-handler";
import { Main } from "./navigation/Main";
import { navigationRef } from "./navigation/RootNavigation";
import 'react-native-gesture-handler';

/* REDUX */ 
import { Provider } from "react-redux";
import store from "./redux/storeConfig";
import COLOR from "./constants/colors";

export default class App extends React.Component{
  
  render() {
    LogBox.ignoreLogs(['Reanimated 2']);
    return(
      <>
        <StatusBar
          barStyle={"light-content"}
          translucent={true}
          backgroundColor={COLOR.HEADER_BACKGROUND}
        />
      <View style={{ flex: 1, backgroundColor: COLOR.HEADER_BACKGROUND }}>
         <NavigationContainer ref={navigationRef}>
        <Provider store={store}>
          <Main />
        </Provider>
      </NavigationContainer>
      </View>
     </>
    )
  }
}